import "./styles.css";
import avatar from "../../assets/avatar.svg";

interface NavBarProps {
  sidebarOpen: boolean;
  openSidebar: () => void;
}

const NavBar = ({ sidebarOpen, openSidebar }: NavBarProps) => {
  return (
    <nav className="navbar">
      <div className="nav_icon" onClick={() => openSidebar()}>
        <i className="fa fa-bars"></i>
      </div>

      <div className="navbar__left">
        <a href="#">Subscribers</a>
        <a href="#">Video Management</a>
        <a href="#" className="active_link">
          Admin
        </a>
      </div>
      <div className="navbar__right">
        <a href="#">
          <i className="fa fa-search"></i>
        </a>

        <a href="#">
          <i className="fa fa-clock-o"></i>
        </a>
        <a href="#">
          <img src={avatar} alt="avatar" width="30" />
        </a>
      </div>
    </nav>
  );
};

export default NavBar;
