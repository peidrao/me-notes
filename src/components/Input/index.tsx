import "./styles.css";
import { InputHTMLAttributes } from "react";
type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  label: string;
};

function Input(props: InputProps) {
  return (
    <>
      <label htmlFor={props.id} className="label">
        {props.label}
      </label>
      <input type="text" className="input" {...props} />
    </>
  );
}

export default Input;
