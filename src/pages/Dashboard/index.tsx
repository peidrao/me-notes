import { useState } from "react";
// import "./App.css";
import NavBar from "../../components/NavBar";
import SideBar from "../../components/SideBar";
import Main from "../../components/Main";

function Dashboard() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const openSidebar = () => {
    setSidebarOpen(true);
  };

  const closeSidebar = () => {
    setSidebarOpen(false);
  };

  return (
    <div className="container">
      <NavBar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
      <Main />
      <SideBar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
    </div>
  );
}

export default Dashboard;
