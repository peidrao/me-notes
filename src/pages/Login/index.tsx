import { FormEvent, useState } from "react";
import Input from "../../components/Input";
import "./styles.css";
import AuthenticationService from "../../services/AuthenticationService";
import useAuth from "../../hooks/useAuth";

const Login = () => {
  const { signin } = useAuth();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const service = new AuthenticationService();
    service
      .login(username, password)
      .then(({ data }) => {
        signin(data.access_token, data.refresh_token);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  return (
    <div className="container-login">
      <form className="form-container" onSubmit={handleSubmit}>
        <h1 className="primary-title">Faça seu login</h1>
        <fieldset className="input-group">
          <Input
            label="Nome de usuario"
            type="text"
            placeholder="Informe seu usuario"
            value={username}
            onChange={(e) => setUsername(e.currentTarget.value)}
          />
        </fieldset>

        <fieldset className="input-group">
          <Input
            label="Senha"
            type="password"
            placeholder="Informe sua senha"
            value={password}
            onChange={(e) => setPassword(e.currentTarget.value)}
          />
        </fieldset>
        <button className="button-submit" type="submit">
          Entrar
        </button>
      </form>
    </div>
  );
};

export default Login;
