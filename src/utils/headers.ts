import { ACCESS_TOKEN_KEY } from "./constants";

export const headers = {
  Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN_KEY)}`,
};
