import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import useAuth from "./hooks/useAuth";
import { AuthProvider } from "./contexts/AuthContexts";

export function RequireAuth({ children }: { children: JSX.Element }) {
  const auth = useAuth();
  const location = useLocation();

  if (auth.isAuthenticated) {
    return children;
  } else {
    return <Navigate to="/" state={{ from: location }} replace />;
  }
}

export function RedirectOnAuth({ children }: { children: JSX.Element }) {
  const auth = useAuth();
  const location = useLocation();
  if (auth.isAuthenticated) {
    return <Navigate to="/dashboard" state={{ from: location }} replace />;
  }
  return children;
}

function MainRouter() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Routes>
          <Route
            index
            path="/"
            element={
              <RedirectOnAuth>
                <Login />
              </RedirectOnAuth>
            }
          />
          <Route
            path="dashboard"
            element={
              <RequireAuth>
                <Dashboard />
              </RequireAuth>
            }
          ></Route>
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default MainRouter;
