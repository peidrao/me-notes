import { createContext, PropsWithChildren } from "react";
import useProvideAuthentication from "../hooks/useProvideAuthentication";

export const AuthContext = createContext({
  isAuthenticated: true,
  signin: (a: string, b: string) => {
    console.log(a, b);
  },
  signout: () => {
    console.log("1");
  },
});

export function AuthProvider({ children }: PropsWithChildren) {
  const auth = useProvideAuthentication();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}
