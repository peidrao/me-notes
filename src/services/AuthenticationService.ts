import { api } from "../utils/api";

export default class AuthenticationService {
  async login(
    username: string,
    password: string
  ): Promise<{
    status: number;
    data: { access_token: string; refresh_token: string };
  }> {
    const response = await api.post("/login", { username, password });
    return {
      status: response.status,
      data: response.data,
    };
  }
}
